# EzLheReader

simple interface to read LHE output from MadGraph <br>
reads *events.lhe.gz file directly

returns a list of TParticles, either
   * a list of all particles when iterating over each event, or
   * a list of all particles matching a PDG_ID (combined list from all events)

Requires PyROOT

See the notebook file for usage examples
