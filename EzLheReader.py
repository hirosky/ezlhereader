# simple interface to read LHE output from MadGraph
# reads events.gz file directly

import ROOT as R
import gzip, os
import xml.etree.ElementTree as ET
from math import sqrt

class EzLheReader:
    # The reader will scan the header of the lhe file for undefined particles
    # if any are found, they will be added to ROOT's TDatabasePDG.Instance()
    # Note: only partial information will be added: PID, mass, name
    def __init__(self, file=None, quiet=False):
        self.quiet=quiet
        if file==None:
            self.print("No file entered, use SetFile")
        else: self.SetFile(file)
               
    def SetFile(self,file):
        self.file=file
        if not os.path.exists(self.file):
            print("EzLheReader: input file not found",self.file)
        
        self.fp=gzip.open(self.file)
        self.xmlroot = ET.fromstring(self.fp.read())
       
        
        slha=self.xmlroot.find("header/slha").text # returns header data as string
        genInfo=self.xmlroot.find("header/MGGenerationInfo").text.split('\n')
        for line in genInfo:
            line=line.split()
            if "Events" in line: self.nevents=int(line[-1])
            if "weight" in line: self.xs=float(line[-1])

        self.print("opening file",file)
        self.print("nevents:", self.nevents)
        self.print("xs:",self.xs,"(pb)")
        self.L_eff=self.nevents/self.xs  # pb-1

        pdg = R.TDatabasePDG.Instance()
        # scan the mass block for new particles
        scan=False
        for line in slha.split("\n"):
            if "BLOCK MASS" in line: scan=True
            elif scan:
                if line.startswith('#'): break
                pid,mass,c,name=line.strip().split()[0:4]
                pid=int(pid)
                mass=float(mass)
                if pdg.GetParticle(pid)==None:
                    self.print("Adding new particle",name,"with id",pid,"mass=",mass,"GeV")
                    pdg.AddParticle(name,name,mass,0,0,0,"",pid)
                    pdg.AddAntiParticle(name+"_bar",-pid)

    # simple logging tool
    def print(self, arg1, *vartuple):
        if self.quiet: return
        msg=arg1
        for var in vartuple: msg=f"{msg} {var}"
        print(msg)
        
          
    # convert MG5 line to TParticle
    def mg2particle(self,line):
        d=line.split()
        # TParticle (pdg, status, mother1, mother2, daughter1, daughter2, px, py, pz, etot, vx, vy, vz, time)
        tp=R.TParticle(int(d[0]),int(d[1]),int(d[2]),int(d[3]),0,0,
                       float(d[6]),float(d[7]),float(d[8]),float(d[9]),
                       0,0,0,0)
        return tp

    # "row-wise" access, get particles per event
    # use generator to read events
    # returns event weight and list of TParticles
    def GetEvents(self):
        for e in self.xmlroot.iter('event'):
            particledata=e.text.split("\n")
            particledata.pop(0) # drop first (empty) element in list
            nparticles=int(particledata[0].split()[0])
            weight=float(particledata[0].split()[2])
            particledata.pop(0)  # drop the event header line
            particles=[None]*nparticles
            for i in range(0,nparticles):
                particles[i]=self.mg2particle( particledata[i] )
            yield particles,weight

    # "column-wise" access, get a list of one particle type across events
    # optionally require a status -1: incoming, 2 intermediate, 1 final state, 0 any
    # return as list of TParticles, selected by PID
    def GetParticles(self,pdgID,status=1):
        plist=[]
        for e in self.xmlroot.iter('event'):
            particledata=e.text.split("\n")
            particledata.pop(0) # drop first (empty) element in list
            nparticles=int(particledata[0].split()[0])
            particles=[None]*nparticles
            weight=float(particledata[0].split()[2])
            particledata.pop(0)  # drop the event header line
            for i in range(0,nparticles):
                part=self.mg2particle( particledata[i] )
                if part.GetStatusCode() != status: continue
                pID=part.GetPdgCode()
                match=False
                if pdgID=='j':  # special case
                    match = abs(pID)<8 or pID==9 or pID==21
                elif pID == pdgID: match=True
                if not match: continue
                plist.append(part)
        #print('retrieved',len(plist),'particles of type ID',pdgID);
        return plist

    def SavePartclesAsTree(self,fname="test.root",tname="particles tree"):
        tf=R.TFile(fname,"recreate")
        tree=R.TTree("tree",tname)
        #ipars=R.vector(R.TParticle)()
        #vpars=R.vector(R.TParticle)()
        fpars=R.vector(R.TParticle)()
        tree.Branch("fpars",fpars)
        for event,weight in self.GetEvents():
            fpars.clear()
            for ptcl in event:
                #ptcl.Print()
                if ptcl.GetStatusCode()==1: fpars.push_back(ptcl)
            tree.Fill()
        tf.Write()
